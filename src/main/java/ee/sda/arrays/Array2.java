package ee.sda.arrays;

public class Array2 {

    public int sumArrayValues(int[] array) {
        int sum = 0;
        for (int i : array) sum += i;
        return sum;
    }

}
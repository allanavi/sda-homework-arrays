package ee.sda.arrays;

import java.util.Arrays;

public class Array18 {

    public int getSecondSmallest(int[] array) {
        Arrays.sort(array);
        return array[1];
    }
}
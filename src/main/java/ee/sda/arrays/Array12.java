package ee.sda.arrays;

import java.util.HashSet;
import java.util.Set;

public class Array12 {

    public int[] findDuplicates(int[] array) {
        Set<Integer> duplicateSet = new HashSet<>();
        for (int i = 0; i < array.length - 1; i++) {
            for (int j = i + 1; j < array.length; j++) {
                if (array[i] == array[j]) duplicateSet.add(array[j]);
            }
        }
        int i = 0;
        int[] newArray = new int[duplicateSet.size()];
        for (Integer j : duplicateSet) {
            newArray[i++] = j;
        }
        return newArray;
    }

}
package ee.sda.arrays;

import java.util.HashSet;
import java.util.Set;

public class Array25 {

    public String[] findCommonElements (String[] array1, String[] array2, String[] array3) {
        Set<String> commonSet = new HashSet<>();
        for (String a1 : array1) {
            for (String a2 : array2) {
                if(a1 != null && a2 != null && a1.equals(a2)) {
                    for (String a3 : array3) {
                        if (a3 != null && a2.equals(a3)) {
                            commonSet.add(a3);
                        }
                    }
                }
            }
        }
        String[] commonArray = commonSet.toArray(new String[commonSet.size()]);
        return commonArray;
    }

}
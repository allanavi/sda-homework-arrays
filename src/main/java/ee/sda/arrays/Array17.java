package ee.sda.arrays;

import java.util.Arrays;

public class Array17 {

    public int getSecondLargest(int[] array) {
        Arrays.sort(array);
        return array[array.length - 2];
    }

}
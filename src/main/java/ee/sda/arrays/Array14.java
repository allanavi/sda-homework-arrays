package ee.sda.arrays;

import java.util.HashSet;
import java.util.Set;

public class Array14 {

    public String[] findCommonValues(String[] array1, String[] array2) {
        Set<String> duplicateSet = new HashSet<>();
        for (String str1 : array1) {
            for (String str2 : array2) {
                if (str1 != null && str2 != null && str1.equals(str2)) duplicateSet.add(str1);
            }
        }
        String[] newArray = duplicateSet.toArray(new String[duplicateSet.size()]);
        return newArray;
    }

}
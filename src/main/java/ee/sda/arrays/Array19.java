package ee.sda.arrays;

import java.util.Arrays;

public class Array19 {

    public void addMatrices(int[][] array1, int[][] array2) {
        int[][] newArray = new int[array1.length][array1[0].length];
        for (int i = 0; i < array1.length; i++) {
            for (int j = 0; j < array1[i].length; j++) {
                newArray[i][j] = array1[i][j] + array2[i][j];
            }
        }
        for (int i = 0; i < array1.length; i++) {
            for (int j = 0; j < array1[i].length; j++) {
                System.out.print(newArray[i][j] + " ");
            }
            System.out.println();
        }
    }

}
package ee.sda.arrays;

import java.util.HashSet;
import java.util.Set;

public class Array13 {

    public String[] findDuplicates(String[] array) {
        Set<String> duplicateSet = new HashSet<>();
        for (int i = 0; i < array.length - 1; i++) {
            for (int j = i + 1; j < array.length; j++) {
                if (array[i] != null && array[j] != null && array[i].equals(array[j])) duplicateSet.add(array[j]);
            }
        }
        int i = 0;
        String[] newArray = new String[duplicateSet.size()];
        for (String j : duplicateSet) {
            newArray[i++] = j;
        }
        return newArray;
    }

}
package ee.sda.arrays;

import java.util.ArrayList;
import java.util.List;

public class Array20 {

    public List<String> arrToArrList(String[] array) {
        List<String> list = new ArrayList<>();
        for (String str : array) {
            if (str != null) list.add(str);
        }
        return list;
    }

}
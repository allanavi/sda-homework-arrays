package ee.sda.arrays;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class Array24 {

    public int[] findMissing(int[] array1, int[] array2) {
        boolean notFound;
        List<Integer> arr1List = Arrays.stream(array1).boxed().collect(Collectors.toList());
        List<Integer> arr2List = Arrays.stream(array2).boxed().collect(Collectors.toList());
        List<Integer> missingList = new ArrayList<>();
        for (int i = 0; i < arr1List.size(); i++) {
            notFound = true;
            for (int j = 0; j < arr2List.size(); j++) {
                if (arr1List.get(i).equals(arr2List.get(j))) {
                    arr2List.remove(j);
                    notFound = false;
                }
            }
            if (notFound) missingList.add(array1[i]);
        }
        int[] missingArray = missingList.stream().mapToInt(Integer::intValue).toArray();
        return missingArray;
    }

}
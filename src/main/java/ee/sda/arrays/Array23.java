package ee.sda.arrays;

public class Array23 {

    public boolean testEquals(String[] array1, String[] array2) {
        boolean areEqual = true;
        for (int i = 0; i < array1.length; i++) {
            if (array1[i] == null ^ array2[i] == null) areEqual = false;
            if (array1[i] != null && array2[i] != null) {
                if (!array1[i].equals(array2[i])) areEqual = false;
            }
        }
        return areEqual;
    }

}
package ee.sda.arrays;

public class Array5 {

    public boolean containsValue(String[] array, String value) {
        for (String str : array) {
            if (str != null && str.equals(value)) return true;
        }
        return false;
    }
}
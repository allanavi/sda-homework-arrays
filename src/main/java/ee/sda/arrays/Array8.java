package ee.sda.arrays;

public class Array8 {

    public String[] copyArray(String[] array) {
        String[] newArray = new String[array.length];
        for (int i = 0; i < array.length; i++) newArray[i] = array[i];
        return newArray;
    }

}
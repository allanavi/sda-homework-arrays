package ee.sda.arrays;

import java.util.HashSet;
import java.util.Set;

public class Array15 {

    public int[] findCommonValues(int[] array1, int[] array2) {
        Set<Integer> duplicateSet = new HashSet<>();
        for (int i1 : array1) {
            for (int i2 : array2) {
                if (i1 == i2) duplicateSet.add(i1);
            }
        }
        int[] newArray = duplicateSet.stream().mapToInt(Integer::intValue).toArray();
        return newArray;
    }

}
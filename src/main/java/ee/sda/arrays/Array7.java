package ee.sda.arrays;

public class Array7 {

    public String[] removeElement(String[] array, String value) {
        String[] newArray = new String[array.length - 1];
        for (int i = 0, j = 0; i < array.length; i++) {
            if (array[i] == null || array[i].equals(value)) continue;
            newArray[j++] = array[i];
        }
        return newArray;
    }

}

package ee.sda.arrays;

import java.util.List;

public class Array21 {

    public String[] arrListToArr(List<String> list) {
        String[] array = list.toArray(new String[list.size()]);
        return array;
    }

}
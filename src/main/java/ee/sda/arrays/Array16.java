package ee.sda.arrays;

import java.util.HashSet;
import java.util.Set;

public class Array16 {

    public String[] removeDuplicates(String[] array) {
        Set<String> singleElements = new HashSet<>();
        for (String str : array) {
            if (str != null) singleElements.add(str);
        }
        String[] newArray = singleElements.toArray(new String[singleElements.size()]);
        return newArray;
    }

}
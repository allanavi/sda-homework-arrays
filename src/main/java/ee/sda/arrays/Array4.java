package ee.sda.arrays;

public class Array4 {

    public double avgValue(int[] array) {
        int total = 0;
        for (int i : array) total += i;
        return (double) total / array.length;
    }

}
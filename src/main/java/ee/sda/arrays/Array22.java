package ee.sda.arrays;

import java.util.ArrayList;
import java.util.List;

public class Array22 {

    public String[] findPairs(int[] array, int sum) {
        List<String> pairsList = new ArrayList<>();
        for (int i = 0; i < array.length - 1; i++) {
            for (int j = i + 1; j < array.length; j++) {
                if (array[i] + array[j] == sum) {
                    pairsList.add("|" + array[i] + " & " + array[j] + "|");
                }
            }
        }
        String[] pairsArray = pairsList.toArray(new String[pairsList.size()]);
        return pairsArray;
    }

}
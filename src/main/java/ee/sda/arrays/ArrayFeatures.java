package ee.sda.arrays;

import java.util.Scanner;

public class ArrayFeatures {

    public int getArrayLengthFromUser() {
        Scanner sc = new Scanner(System.in);
        String userInput;
        int arrayLength;

        System.out.println("Choose how many integers or words you would like to enter.\n"
                + "If you do not wish to continue, do not enter anything and press ENTER.\n");

        while (true) {
            System.out.print("Enter an integer and press ENTER: ");

            userInput = sc.nextLine();
            if (userInput.equals("")) System.exit(0);

            try {
                arrayLength = Integer.parseInt(userInput);
                if (arrayLength < 2) {
                    System.out.println("The number has to be at least 2.\n");
                    continue;
                }
                if (arrayLength > (Integer.MAX_VALUE - 5)) {
                    System.out.println("This is too much! Try a smaller number.\n");
                    continue;
                }
                System.out.println("Thank you!\n");
                break;
            }
            catch (NumberFormatException e) {
                System.out.println("This is not a proper integer!\n");
            }
        }
        return arrayLength;
    }

    public String[] getStringArrayFromUser(int arrayLength) {
        String[] array = new String[arrayLength];
        Scanner sc = new Scanner(System.in);
        String userInput;
        System.out.println("Now you can start adding numbers/words to be sorted one by one.\n"
                + "Press ENTER after each object.\n");

        for (int i = 0; i < array.length; i++) {
            System.out.print("Enter either integers or words. When done, press ENTER: ");
            userInput = sc.nextLine();

            if (!userInput.equals("")) {
                array[i] = userInput;
            }
            else break;
        }
        return array;
    }

    public int[] getIntArrayFromUser(int arrayLength) {
        int[] array = new int[arrayLength];
        Scanner sc = new Scanner(System.in);
        String userInput;
        System.out.println("Now you can start to add integers to the array.\n"
                + "Press ENTER after each integer.\n");

        for (int i = 0; i < array.length; i++) {
            System.out.print("Provide an integer and press ENTER: ");
            userInput = sc.nextLine();

            if (!userInput.equals("")) {
                try {
                    array[i] = Integer.parseInt(userInput);
                }
                catch (NumberFormatException e) {
                    System.out.println("This was not a proper integer!\n");
                    i--;
                }
            }
            else break;
        }
        return array;
    }

    public void printArray(String[] array) {
        for (String str : array) {
            if (str == null) break;
            System.out.print(str + " ");
        }
        System.out.println();
    }

    public void printArray(int[] array) {
        for (int i : array) {
           System.out.print(i + " ");
        }
        System.out.println();
    }

    public String getStringValueFromUser() {
        Scanner sc = new Scanner(System.in);
        System.out.print("\nEnter something and press ENTER: ");
        String userInput = sc.nextLine();
        return userInput;
    }

    public int getIntValueFromUser() {
        Scanner sc = new Scanner(System.in);
        String userInput;
        System.out.print("\nEnter an integer and press ENTER: ");
        userInput = sc.nextLine();
        try {
            return Integer.parseInt(userInput);
        }
        catch (NumberFormatException e) {
            System.out.println("This was not a proper integer!\n");
            return -1;
        }
    }

}
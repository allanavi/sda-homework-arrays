package ee.sda.arrays;

public class Array9 {

    public String[] addElement(String[] array, int index, String newValue) {
        String[] newArray = new String[array.length + 1];
        if (index > array.length) {
            System.out.println("The position index is too large for this array! Cannot edit the array!\nArray:");
            return array;
        }
        for (int i = 0, j = 0; i < array.length; i++) {
            if (j == index) newArray[j++] = newValue;
            newArray[j++] = array[i];
        }
        return newArray;
    }

}
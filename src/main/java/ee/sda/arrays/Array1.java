package ee.sda.arrays;

public class Array1 {
    public String[] sortArray(String[] array) {
        for (int i = 0; i < array.length - 1; i++) {
            if (array[i] == null) break;
            int smallestIndex = i;
            for (int j = i + 1; j < array.length; j++) {
                if (array[j] == null) break;
                if (jSmaller(array, i, j) && jSmaller(array, smallestIndex, j)) smallestIndex = j;
            }
            String temp = array[smallestIndex];
            array[smallestIndex] = array[i];
            array[i] = temp;
        }
        return array;        
    }
    
    private boolean jSmaller(String[] array, int i, int j) {
        if (isIntArray(array)) {
            return Integer.parseInt(array[i]) > Integer.parseInt(array[j]);
        }
        return array[i].compareToIgnoreCase(array[j]) > 0;
    }

    private boolean isIntArray(String[] array) {
        try {
            for (String str : array) {
                if (str == null) break;
                Integer.parseInt(str);
            }
            return true;
        }
        catch (NumberFormatException e) {
            return false;
        }
    }

}
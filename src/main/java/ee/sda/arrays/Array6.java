package ee.sda.arrays;

public class Array6 {

    public int findIndex(String[] array, String value) {
        for (int i = 0; i < array.length; i++) {
            if (array[i] != null && array[i].equals(value)) return i;
        }
        return -1;
    }

}
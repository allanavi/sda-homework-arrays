package ee.sda.arrays;

import java.util.Arrays;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        ArrayFeatures aFeatures = new ArrayFeatures();

////      Array1
//        String[] array = aFeatures.getStringArrayFromUser(aFeatures.getArrayLengthFromUser());
//        System.out.println("\nHere is the array you provided:");
//        aFeatures.printArray(array);
//        Array1 arraySorter = new Array1();
//        arraySorter.sortArray(array);
//        System.out.println("And here is the sorted array:");
//        aFeatures.printArray(array);

////      Array2
//        int[] array = aFeatures.getIntArrayFromUser(aFeatures.getArrayLengthFromUser());
//        Array2 arraySum = new Array2();
//        System.out.println("\nThe sum of the array you provided is " + arraySum.sumArrayValues(array) + ".");

////      Array3
//        Array3 gridPrinter = new Array3();
//        gridPrinter.printGrid();

////      Array4
//        int[] array = aFeatures.getIntArrayFromUser(aFeatures.getArrayLengthFromUser());
//        Array4 arrayAverage = new Array4();
//        System.out.println("\nThe average value of the array you provided is " + arrayAverage.avgValue(array) + ".");

////      Array5
//        String[] array = aFeatures.getStringArrayFromUser(aFeatures.getArrayLengthFromUser());
//        String value = aFeatures.getStringValueFromUser();
//        Array5 matchFinder = new Array5();
//        System.out.println(matchFinder.containsValue(array, value) ?
//                                "\nThe value is in the array." :
//                                "\nThe value is not in the array.");

////      Array6
//        String[] array = aFeatures.getStringArrayFromUser(aFeatures.getArrayLengthFromUser());
//        String value = aFeatures.getStringValueFromUser();
//        Array6 indexFinder = new Array6();
//        int foundIndex = indexFinder.findIndex(array, value);
//        System.out.println(foundIndex >= 0 ?
//                                "\nThe (first) index of the value in the array is " + foundIndex + "." :
//                                "\nThe value is not in the array.");

////      Array7
//        String[] array = aFeatures.getStringArrayFromUser(aFeatures.getArrayLengthFromUser());
//        String value = aFeatures.getStringValueFromUser();
////      Let's use a previously created class to check if the value is in the array!
//        Array5 matchFinder = new Array5();
//        if (!matchFinder.containsValue(array, value)) System.out.println("The value is not in the array.");
//        else {
//            Array7 elementRemover = new Array7();
//            System.out.println("\nHere is the array you provided:");
//            aFeatures.printArray(array);
//            System.out.println("And here is the array without the element you specified:");
//            aFeatures.printArray(elementRemover.removeElement(array, value));
//        }

////      Array8
//        String[] array = aFeatures.getStringArrayFromUser(aFeatures.getArrayLengthFromUser());
//        Array8 arrayCopier = new Array8();
//        System.out.println("\nHere is the array you provided:");
//        aFeatures.printArray(array);
//        System.out.println("And here is a copy of the same array:");
//        aFeatures.printArray(arrayCopier.copyArray(array));

////      Array9
//        String[] array = aFeatures.getStringArrayFromUser(aFeatures.getArrayLengthFromUser());
//        int index = aFeatures.getIntValueFromUser();
//        String value = aFeatures.getStringValueFromUser();
//        Array9 elementAdder = new Array9();
//        System.out.println("\nHere is the array you provided:");
//        aFeatures.printArray(array);
//        System.out.println("And here is the array with the added element:");
//        aFeatures.printArray(elementAdder.addElement(array, index, value));

////      Array10
//        int[] array = aFeatures.getIntArrayFromUser(aFeatures.getArrayLengthFromUser());
//        Array10 maxMinGetter = new Array10();
//        System.out.println();
//        System.out.println("The maximum value of your array is " + maxMinGetter.maxValue(array) + ".");
//        System.out.println("The minimum value of your array is " + maxMinGetter.minValue(array) + ".");

////      Array11
//        int[] array = aFeatures.getIntArrayFromUser(aFeatures.getArrayLengthFromUser());
//        Array11 arrayReverser = new Array11();
//        System.out.println("\nHere is the array you provided:");
//        aFeatures.printArray(array);
//        System.out.println("And here is reversed order:");
//        aFeatures.printArray(arrayReverser.reverseArray(array));

////      Array12
//        int[] array = aFeatures.getIntArrayFromUser(aFeatures.getArrayLengthFromUser());
//        Array12 duplicateFinder = new Array12();
//        int[] duplicates = duplicateFinder.findDuplicates(array);
//        System.out.println("\nHere is the array you provided:");
//        aFeatures.printArray(array);
//        System.out.println("And here are the duplicate values:");
//        aFeatures.printArray(duplicates);

////      Array13
//        String[] array = aFeatures.getStringArrayFromUser(aFeatures.getArrayLengthFromUser());
//        Array13 duplicateFinder = new Array13();
//        String[] duplicates = duplicateFinder.findDuplicates(array);
//        System.out.println("\nHere is the array you provided:");
//        aFeatures.printArray(array);
//        System.out.println("And here are the duplicate values:");
//        aFeatures.printArray(duplicates);

////      Array14
//        System.out.println("First, you get to enter the first array!\n");
//        String[] array1 = aFeatures.getStringArrayFromUser(aFeatures.getArrayLengthFromUser());
//        System.out.println("\nNow, please enter the second array!\n");
//        String[] array2 = aFeatures.getStringArrayFromUser(aFeatures.getArrayLengthFromUser());
//        System.out.println("\nHere is the first array you provided:");
//        aFeatures.printArray(array1);
//        System.out.println("And here is the second one:");
//        aFeatures.printArray(array2);
//        Array14 commonValueFinder = new Array14();
//        System.out.println("\nHere are the common values between the two arrays:");
//        aFeatures.printArray(commonValueFinder.findCommonValues(array1, array2));

////      Array15
//        System.out.println("First, you get to enter the first array!\n");
//        int[] array1 = aFeatures.getIntArrayFromUser(aFeatures.getArrayLengthFromUser());
//        System.out.println("\nNow, please enter the second array!\n");
//        int[] array2 = aFeatures.getIntArrayFromUser(aFeatures.getArrayLengthFromUser());
//        Array15 commonValueFinder = new Array15();
//        System.out.println("\nHere are the common values between the two arrays:");
//        aFeatures.printArray(commonValueFinder.findCommonValues(array1, array2));

////      Array16
//        String[] array = aFeatures.getStringArrayFromUser(aFeatures.getArrayLengthFromUser());
//        Array16 duplicateRemover = new Array16();
//        System.out.println("\nHere is the array you provided:");
//        aFeatures.printArray(array);
//        System.out.println("And here is the array without duplicate values:");
//        aFeatures.printArray(duplicateRemover.removeDuplicates(array));

////      Array17
//        int[] array = aFeatures.getIntArrayFromUser(aFeatures.getArrayLengthFromUser());
//        Array17 secondLargestElement = new Array17();
//        System.out.println("\nHere is the array you provided:");
//        aFeatures.printArray(array);
//        System.out.print("And here is the second largest element of the array: "
//                            + secondLargestElement.getSecondLargest(array) + ".");

////      Array18
//        int[] array = aFeatures.getIntArrayFromUser(aFeatures.getArrayLengthFromUser());
//        Array18 secondSmallestElement = new Array18();
//        System.out.println("\nHere is the array you provided:");
//        aFeatures.printArray(array);
//        System.out.print("And here is the second smallest element of the array: "
//                            + secondSmallestElement.getSecondSmallest(array) + ".");

////      Array19
//        int[][] array1 = {{1, 2, 3}, {4, 5, 6}, {7, 8, 9}};
//        int[][] array2 = {{9, 8, 7}, {6, 5, 4}, {3, 2, 1}};
//        Array19 matrixAdder = new Array19();
//        System.out.println("Here is the result of adding two matrices:");
//        matrixAdder.addMatrices(array1, array2);

////      Array20
//        String[] array = aFeatures.getStringArrayFromUser(aFeatures.getArrayLengthFromUser());
//        Array20 arrayToListConverter = new Array20();
//        List<String> list = arrayToListConverter.arrToArrList(array);
//        System.out.println("\nHere is the array as a list: ");
//        list.forEach(System.out::println);

////      Array21
//        String[] array = aFeatures.getStringArrayFromUser(aFeatures.getArrayLengthFromUser());
//        Array20 arrayToListConverter = new Array20();
//        Array21 listToArrayConverter = new Array21();
//        List<String> list = arrayToListConverter.arrToArrList(array);
//        array = listToArrayConverter.arrListToArr(list);
//        System.out.println("\nHere is the array you provided converted ta a list and back to an array:");
//        aFeatures.printArray(array);

////      Array22
//        int[] array = aFeatures.getIntArrayFromUser(aFeatures.getArrayLengthFromUser());
//        System.out.print("\nNow, choose what the sum of two integers to be paired up is going to be!");
//        int sum = aFeatures.getIntValueFromUser();
//        Array22 pairFinder = new Array22();
//        System.out.println("\nHere is the array you provided:");
//        aFeatures.printArray(array);
//        System.out.println("And here are the integer pairs that add up to "+ sum + ":");
//        aFeatures.printArray(pairFinder.findPairs(array, sum));

////      Array23
//        System.out.println("First, you get to enter the first array!\n");
//        String[] array1 = aFeatures.getStringArrayFromUser(aFeatures.getArrayLengthFromUser());
//        System.out.println("\nNow, please enter the second array!\n");
//        String[] array2 = aFeatures.getStringArrayFromUser(aFeatures.getArrayLengthFromUser());
//        System.out.println("\nHere is the first array you provided:");
//        aFeatures.printArray(array1);
//        System.out.println("And here is the second one:");
//        aFeatures.printArray(array2);
//        Array23 equalityTester = new Array23();
//        System.out.print("\nThe two arrays ");
//        System.out.println(equalityTester.testEquals(array1, array2) ? "are equal." : "are not equal.");

////      Array24
//        System.out.println("First, you get to enter the first array!\n");
//        int[] array1 = aFeatures.getIntArrayFromUser(aFeatures.getArrayLengthFromUser());
//        System.out.println("\nNow, please enter the second array!\n");
//        int[] array2 = aFeatures.getIntArrayFromUser(aFeatures.getArrayLengthFromUser());
//        System.out.println("\nHere is the first array you provided:");
//        aFeatures.printArray(array1);
//        System.out.println("And here is the second one:");
//        aFeatures.printArray(array2);
//        System.out.println("\nHere are the numbers that were present in the first, but not in the second array:");
//        Array24 missingFinder = new Array24();
//        aFeatures.printArray(missingFinder.findMissing(array1, array2));

////      Array25
//        System.out.println("First, you get to enter the first array!\n");
//        String[] array1 = aFeatures.getStringArrayFromUser(aFeatures.getArrayLengthFromUser());
//        System.out.println("\nNow, please enter the second array!\n");
//        String[] array2 = aFeatures.getStringArrayFromUser(aFeatures.getArrayLengthFromUser());
//        System.out.println("\nAnd the third!\n");
//        String[] array3 = aFeatures.getStringArrayFromUser(aFeatures.getArrayLengthFromUser());
//        Array1 arraySorter = new Array1();
//        arraySorter.sortArray(array1);
//        arraySorter.sortArray(array2);
//        arraySorter.sortArray(array3);
//        System.out.println("\nHere is the first array you provided (sorted):");
//        aFeatures.printArray(array1);
//        System.out.println("The second one (sorted):");
//        aFeatures.printArray(array2);
//        System.out.println("And the third (sorted):");
//        aFeatures.printArray(array3);
//        Array25 commonElementFinder = new Array25();
//        System.out.println("\nHere are the common elements of all three arrays:");
//        aFeatures.printArray(commonElementFinder.findCommonElements(array1, array2, array3));

    }
}